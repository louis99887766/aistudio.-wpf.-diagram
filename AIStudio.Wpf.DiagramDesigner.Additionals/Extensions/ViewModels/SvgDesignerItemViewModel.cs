﻿using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Models;

namespace AIStudio.Wpf.DiagramDesigner.Additionals.Extensions.ViewModels
{
    public class SvgDesignerItemViewModel: MediaItemViewModel
    {
        protected override string Filter { get; set; } = "Svg|*.svg";

        public SvgDesignerItemViewModel() : base()
        {
        }

        public SvgDesignerItemViewModel(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {

        }

        public SvgDesignerItemViewModel(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }

        public override SelectableItemBase GetSerializableObject()
        {
            return new MediaDesignerItem(this);
        }
    }
}
