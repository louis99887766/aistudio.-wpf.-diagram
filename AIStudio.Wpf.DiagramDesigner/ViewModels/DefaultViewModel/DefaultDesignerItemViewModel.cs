﻿using System;
using System.ComponentModel;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Models;
using AIStudio.Wpf.DiagramDesigner.Services;

namespace AIStudio.Wpf.DiagramDesigner
{
    /// <summary>
    /// DefaultNode
    /// </summary>
    public class DefaultDesignerItemViewModel : DesignerItemViewModelBase
    {
        public DefaultDesignerItemViewModel() : this(null)
        {

        }

        public DefaultDesignerItemViewModel(IDiagramViewModel root) : base(root)
        {

        }

        public DefaultDesignerItemViewModel(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {

        }

        public DefaultDesignerItemViewModel(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }

        public override SelectableItemBase GetSerializableObject()
        {
            return new DesignerItemBase(this);
        }

    }
}
