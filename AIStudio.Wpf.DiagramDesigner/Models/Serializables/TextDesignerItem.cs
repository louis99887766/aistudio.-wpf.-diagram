﻿using AIStudio.Wpf.DiagramDesigner;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class TextDesignerItem : DesignerItemBase
    {
        public TextDesignerItem()
        {

        }
        public TextDesignerItem(TextDesignerItemViewModel item) : base(item)
        {
            this.Text = item.Text;
        }

    }
}
